package com.fomin.enums;

/**
 * Created by Fomin
 */

/**
 * Enumeration for color tones
 */
public enum EFlowerColor {
    WHITE(0), // Білий
    BLACK(1), // Чорний
    RED(2), // Червоний
    YELLOW(3), // Жовтий
    OTHER_COLOR(4); // Інший колір

    private int index;

    EFlowerColor(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    /**
     * Method returns color name
     * @param fc parameter contains a constant by which to return the color name
     * @return returns the color name
     */
    public static String getColorNameByColor(EFlowerColor fc) {
        String nameColor = "Не знайдений";

        switch (fc) {
            case WHITE:
                nameColor = "Білий";
                break;
            case BLACK:
                nameColor = "Чорний";
                break;
            case RED:
                nameColor = "Червоний";
                break;
            case YELLOW:
                nameColor = "Жовтий";
                break;
            case OTHER_COLOR:
                nameColor = "Інший колір";
                break;
            default:
        }

        return nameColor;
    }

}
