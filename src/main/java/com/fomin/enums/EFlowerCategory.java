package com.fomin.enums;

/**
 * Created by Fomin
 */

/**
 * Enumeration of categories of colors
 */
public enum EFlowerCategory {
    ROSE(0), // Роза
    LILIA(1), // Лілія
    FIALKA(2), // Фіалка
    ASTRA(3), // Астра
    OTHER_CATEGORY(4); // Інша категорія

    private int index;

    EFlowerCategory (int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    /**
     * The method returns the name of the color category
     * @param fc parameter contains a constant by which to return the category name
     * @return returns the name of a color category
     *      
     */
    public static String getNameByCategory(EFlowerCategory fc) {
        String nameCategory = "Не знайдено";

        switch (fc) {
            case ROSE:
                nameCategory = "Роза";
                break;
            case LILIA:
                nameCategory = "Лілія";
                break;
            case FIALKA:
                nameCategory = "Фіалка";
                break;
            case ASTRA:
                nameCategory = "Астра";
                break;
            case OTHER_CATEGORY:
                nameCategory = "Інші квіти";
                break;
            default:
        }

        return nameCategory;
    }

}