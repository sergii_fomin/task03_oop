package com.fomin;

/**
 * Created by Fomin
 */

import com.fomin.classes.FlowersBasketsBouquetsFabric;

import static com.fomin.utils.ApplicationUtils.*;
import static com.fomin.utils.PropertiesFileUtils.*;

public class Main {
    /**
     * Application launch method
     * @param args parameter is not processed when the application starts            
     */

    public static void main(String[] args) {
        // load auto parameters to run the application in a file of type Properties
        savePropertiesStartupToFile("load.properties");

        // create a factory of objects for the application to work
        FlowersBasketsBouquetsFabric myFabric = createMyFabric();

        // demonstration of working with a list of objects containing color baskets
        workBasketsByMyFabric(myFabric);

        // demonstration of working with a list of objects containing bouquets of flowers
        workBouquetsByMyFabric(myFabric);

    }
}