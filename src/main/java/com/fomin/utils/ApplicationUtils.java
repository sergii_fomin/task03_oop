package com.fomin.utils;

/**
 * Created by Fomin
 */

import com.fomin.classes.Flower;
import com.fomin.classes.FlowersBasket;
import com.fomin.classes.FlowersBasketsBouquetsFabric;
import com.fomin.classes.FlowersBouquet;
import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;

import java.util.Collections;

import static com.fomin.utils.PropertiesFileUtils.loadFlowersToBasketByPropertiesFile;
import static com.fomin.classes.FlowersBasketsBouquetsFabric.*;

/**
 * Service class for working with the application
 */
public class ApplicationUtils {

    // create a factory
    public static FlowersBasketsBouquetsFabric createMyFabric() {
        System.out.println("\nCreate a factory of objects for the application");

        return new FlowersBasketsBouquetsFabric();
    }

    // demonstrates how to work with a list of flower baskets using a factory
    public static void workBasketsByMyFabric(FlowersBasketsBouquetsFabric myFabric) {
        // create flower baskets from the loaded data using the factory
        FlowersBasket flowersBasket1 = myFabric.CreateFlowersBasket(1, "Горщик с розами");
        FlowersBasket flowersBasket2 = myFabric.CreateFlowersBasket(2, "Горщик с астрами");
        FlowersBasket flowersBasket3 = myFabric.CreateFlowersBasket(3, "Горщик с ліліями");
        FlowersBasket flowersBasket4 = myFabric.CreateFlowersBasket(4, "Горщик с георгінами");

        // load the flowers into the created baskets from the autorun options

        // loading flowers in 1 basket
        loadFlowersToBasketByPropertiesFile("load.properties", flowersBasket1, "0");
        loadFlowersToBasketByPropertiesFile("load.properties", flowersBasket1, "1");
        // add another rose to this basket through the factory of objects to demonstrate the capabilities of the factory
        Flower flowerRose = myFabric.CreateFlower((flowersBasket1.getFlowersBasketList().size() + 1),
                "Роза голландська", "Червона", EFlowerColor.RED,
                EFlowerCategory.getNameByCategory(EFlowerCategory.ROSE), EFlowerCategory.ROSE, 1500.00f);
        flowersBasket1.addFlower(flowerRose);

        // loading flowers in 2 basket
        loadFlowersToBasketByPropertiesFile("load.properties", flowersBasket2, "2");

        // loading flowers in 3 basket
        loadFlowersToBasketByPropertiesFile("load.properties", flowersBasket3, "3");

        // loading flowers in 4 basket
        loadFlowersToBasketByPropertiesFile("load.properties", flowersBasket4, "4");

        // draw output to the console of the downloaded data
        // to the list of baskets with flowers
        System.out.println("\nDisplaying a NOT sorted by price list of flower baskets");
        myFabric.printBasketsFromListBasket();
        // теперь отсортируем список корзин с цветами по стоимости
        Collections.sort(myFabric.getFlowersBasketList());
        // сделаем вывод списка еще раз
        System.out.println("\nDisplay a list of flower baskets sorted by price.");
        myFabric.printBasketsFromListBasket();
    }

    // show the ability to work with a list of flower bouquets using the factory
    public static void workBouquetsByMyFabric(FlowersBasketsBouquetsFabric myFabric) {

        System.out.println("\nWe create flower bouquets using the factory");

        // bouquet 1
        FlowersBouquet flwBouquet1 = myFabric.CreateFlowersBouquet(1);
        int index = myFabric.searchBasketIndexByCode(myFabric.getFlowersBasketList(), 1);
        FlowersBasket flwBasket1 = myFabric.getFlowersBasketList().get(index);
        addFlowerToBouquet(flwBasket1, flwBouquet1, EFlowerCategory.ROSE, "Роза голландська", EFlowerColor.WHITE, "Біла", 500.00f, 3);
        addFlowerToBouquet(flwBasket1, flwBouquet1, EFlowerCategory.ROSE, "Роза чорна", EFlowerColor.BLACK, "Чорна", 350.00f, 2);

        // bouquet 2
        FlowersBouquet flwBouquet2 = myFabric.CreateFlowersBouquet(2);
        index = myFabric.searchBasketIndexByCode(myFabric.getFlowersBasketList(), 2);
        FlowersBasket flwBasket2 = myFabric.getFlowersBasketList().get(index);
        addFlowerToBouquet(flwBasket2, flwBouquet2, EFlowerCategory.ASTRA, "Астра", EFlowerColor.WHITE, "Біла", 200.00f, 3);
        index = myFabric.searchBasketIndexByCode(myFabric.getFlowersBasketList(), 4);
        FlowersBasket flwBasket3 = myFabric.getFlowersBasketList().get(index);
        addFlowerToBouquet(flwBasket3, flwBouquet2, EFlowerCategory.OTHER_CATEGORY, "Георгіна звичайна", EFlowerColor.OTHER_COLOR, "Бордо", 450.00f, 2);

        // bouquet 3
        FlowersBouquet flwBouquet3 = myFabric.CreateFlowersBouquet(3);
        index = myFabric.searchBasketIndexByCode(myFabric.getFlowersBasketList(), 3);
        FlowersBasket flwBasket4 = myFabric.getFlowersBasketList().get(index);
        addFlowerToBouquet(flwBasket4, flwBouquet3, EFlowerCategory.LILIA, "Лілія китайська", EFlowerColor.OTHER_COLOR, "Біло-розова", 1000.00f, 7);

        // draw output to the console of the downloaded data
        // to the list of bouquets with flowers
        System.out.println("\nWe output a NOT sorted by price list of flower bouquets");
        myFabric.printBouquetFromListBouquet();
        // now sort the list of bouquets with flowers by cost
        Collections.sort(myFabric.getFlowersBouquetList());
        // draw the list again
        System.out.println("\nWe display a sorted by price list of bouquets of flowers");
        myFabric.printBouquetFromListBouquet();

        System.out.println("\nWe display the contents of each bouquet from the list of bouquets");

        for (FlowersBouquet flwBouquet : myFabric.getFlowersBouquetList()) {
            System.out.println(flwBouquet.toString());
            flwBouquet.printFlowersFromBouquet();
        }

        // return 2 rose flowers to the basket flwBasket1 from the bouquet flwBouquet1
        System.out.println("\nWe will return 2 rose flowers from 1 bouquet back to the basket");
        backFlowerToBasket(flwBouquet1, flwBasket1, EFlowerCategory.ROSE, "Роза звичайна", EFlowerColor.BLACK, "Чорна", 35.00f, 2);
        flwBouquet1.printFlowersFromBouquet();
        flwBasket1.printFlowersFromBasket();

        // sort the flowers in 1 basket by price
        System.out.println("\nSort flowers in 1 basket by price\n");
        Collections.sort(flwBasket1.getFlowersBasketList());
        flwBasket1.printFlowersFromBasket();
    }


}
