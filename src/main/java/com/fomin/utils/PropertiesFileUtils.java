package com.fomin.utils;

/**
 * Created by Fomin
 */

import com.fomin.classes.Flower;
import com.fomin.classes.FlowersBasket;
import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;

import java.io.*;
import java.util.Properties;

/**
 *   * Utility class for automatically loading data from a file of a specific format of type Properties
 *  
 */
public class PropertiesFileUtils {

    /**
     * The method loads data for further use from a file of a specific format
     *
     * @param fileName        parameter indicates the name of the file from which data will be downloaded
     * @param flowersToBasket contains a link to an object - a basket of flowers into which data from a file will be loaded
     * @param rowValue        contains the value (integer in text form) of the line number from the file.
     *                        Indicated in each field for each row of data in the file (for example, flowerCategory1
     *                        this is 1 line of data that will be loaded into the flower basket, etc.)
     *                             
     */
    public static void loadFlowersToBasketByPropertiesFile(String fileName, FlowersBasket flowersToBasket, String rowValue) {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(fileName);

            prop.load(input);

            autoLoadFlowersToBasket(prop, flowersToBasket, rowValue);
            flowersToBasket.calculateBasketCost();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Method for loading data (colors) into a basket of colors from a file
     *
     * @param prop     contains data sorted by color categories
     * @param rowValue Service parameter for loading data line by line (for example = 1, then all are loaded
     *                 key parameters that end with 1 in the autorun settings file)
     *                      
     */
    private static void autoLoadFlowersToBasket(Properties prop, FlowersBasket flowersToBasket, String rowValue) {
        // initsializatsiya peremennykh dlya zagruzki
        Integer fCount = Integer.valueOf(prop.getProperty("flowerCount" + rowValue));

        Integer fCategory = 4;
        Integer fColor = 4;

        Flower flw;
        EFlowerCategory flowerCategory;
        EFlowerColor flowerColor;

        int tmpFirstIndex = flowersToBasket.getFlowersBasketList().size() + 1;
        fCount = (tmpFirstIndex - 1) + fCount;

        for (int i = tmpFirstIndex; i <= fCount; i++) {

            fCategory = Integer.valueOf(prop.getProperty("flowerCategory" + rowValue));
            switch (fCategory) {
                case 0:
                    flowerCategory = EFlowerCategory.ROSE;
                    break;
                case 1:
                    flowerCategory = EFlowerCategory.LILIA;
                    break;
                case 2:
                    flowerCategory = EFlowerCategory.FIALKA;
                    break;
                case 3:
                    flowerCategory = EFlowerCategory.ASTRA;
                    break;
                case 4:
                    flowerCategory = EFlowerCategory.OTHER_CATEGORY;
                    break;
                default:
                    flowerCategory = EFlowerCategory.OTHER_CATEGORY;
            }

            fColor = Integer.valueOf(prop.getProperty("flowerColor" + rowValue));
            switch (fColor) {
                case 0:
                    flowerColor = EFlowerColor.WHITE;
                    break;
                case 1:
                    flowerColor = EFlowerColor.BLACK;
                    break;
                case 2:
                    flowerColor = EFlowerColor.RED;
                    break;
                case 3:
                    flowerColor = EFlowerColor.YELLOW;
                    break;
                case 4:
                    flowerColor = EFlowerColor.OTHER_COLOR;
                    break;
                default:
                    flowerColor = EFlowerColor.OTHER_COLOR;
            }

            // use the constructor implemented in the Flower class to load
            flw = new Flower(i, prop.getProperty("flowerName" + rowValue),
                    prop.getProperty("flowerColorName" + rowValue),
                    flowerColor,
                    prop.getProperty("flowerCategoryName" + rowValue),
                    flowerCategory,
                    Float.valueOf(prop.getProperty("flowerCost" + rowValue)));

            // add a flower to the specified basket
            flowersToBasket.addFlower(flw);
        }
    }

    /**
     * Method of preparing data for saving to a file (for further loading from a file)
     *
     * @param prop parameter points to a container with data to save to a file
     * @param rowData parameter contains data in the form of an array for saving to a file
     * ("flowerName" + rowValue = rowData [0]
     * "flowerCategory" + rowValue = rowData [1]
     * "flowerCategoryName" + rowValue = rowData [2]
     * "flowerColor" + rowValue = rowData [3]
     * "flowerColorName" + rowValue = rowData [4]
     * "flowerCount" + rowValue = rowData [5]
     * "flowerCost" + rowValue = rowData [6]
     * @param rowValue parameter points to a data string to be saved to a file
     *      
     */
    private static void saveFlowerCategoryForStartup(Properties prop, String[] rowData, String rowValue) {
        prop.setProperty("flowerName" + rowValue, rowData[0].toString());
        prop.setProperty("flowerCategory" + rowValue, rowData[1].toString());
        prop.setProperty("flowerCategoryName" + rowValue, rowData[2].toString());
        prop.setProperty("flowerColor" + rowValue, rowData[3].toString());
        prop.setProperty("flowerColorName" + rowValue, rowData[4].toString());
        prop.setProperty("flowerCount" + rowValue, rowData[5].toString());
        prop.setProperty("flowerCost" + rowValue, rowData[6].toString());
    }

    /**
     * The method prepares and saves data for program self-tests to a file
     *
     * @param fileName parameter indicates the name of the file in which data will be saved
     */
    public static void savePropertiesStartupToFile(String fileName) {
        System.out.println("\nCreate a data file for startup in the application");

        Properties prop = new Properties();
        OutputStream output = null;

        try {

            output = new FileOutputStream(fileName);

            String[] row0 = {"Роза голландська", "0", "Роза", "0", "Біла", "10", "50.00"};
            saveFlowerCategoryForStartup(prop, row0, "0");
            prop.store(output, null);
            prop.clear();

            String[] row1 = {"Роза чорна", "0", "Роза", "1", "Чорна", "20", "35.00"};
            saveFlowerCategoryForStartup(prop, row1, "1");
            prop.store(output, null);
            prop.clear();

            String[] row2 = {"Астра", "3", "Астра", "0", "Біла", "50", "20.00"};
            saveFlowerCategoryForStartup(prop, row2, "2");
            prop.store(output, null);
            prop.clear();

            String[] row3 = {"Лілія китайська", "1", "Лілія", "4", "Біло-розова", "15", "100.00"};
            saveFlowerCategoryForStartup(prop, row3, "3");
            prop.store(output, null);
            prop.clear();

            String[] row4 = {"Георгіна звичайна", "4", "Георгіна", "4", "Бордо", "30", "45.00"};
            saveFlowerCategoryForStartup(prop, row4, "4");
            prop.store(output, null);
            prop.clear();
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
