package com.fomin.interfaces;

/**
 * Created by Fomin
 */

import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;

/**
 * Extension of the interface for implementation in flower classes. Contains an addition in the form of a flower price field
 */
public interface IFlowerForSale extends IFlower {
    // flower price
    float flowerCost = 0.00f;

    /**
     * The method should return the price of the flower
     * @return should return the text value of the flower category name
     */
    float getFlowerCost();

    /**
     * The method should set the price of the flower
           * @param flowerCost contains the price value of the flower
     */
    void setFlowerCost(float flowerCost);
}
