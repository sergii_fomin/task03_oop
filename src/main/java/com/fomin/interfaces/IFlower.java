package com.fomin.interfaces;

/**
 * Created by Fomin
 */

import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;

public interface IFlower {
    /**
     * Flower code
     *      
     */
    int flowerCode = 0;
    /**
     * Name of flower
     *      
     */
    String flowerName = "";

    /**
     * Flower color
     *      
     */
    EFlowerColor flowerColor = EFlowerColor.OTHER_COLOR;
    /**
     * Flower color in text form
     *      
     */
    String flowerColorName = "";

    /**
     * Flower category
     *      
     */
    EFlowerCategory flowerCategory = EFlowerCategory.OTHER_CATEGORY;
    /**
     *  Flower category in text form
     *      
     */
    String flowerCategoryName = "";

    /**
     * Returns the text value of the flower color name
     * should return the text value of the flower color name
     *  @return should return the text value of the flower color name
     *      
     */

    public String getFlowerColorName();

    /**
     * Sets the text value of the flower color name
     * @param flowerColorName contains the text value of the flower color name
     *      
     */

    public void setFlowerColorName(String flowerColorName);


    /**
     * Returns the text value of a flower category name
     * should return the text value of the flower category name
     * @return should return the text value of the flower category name
     *      
     */

    public String getFlowerCategoryName();

    /**
     * Sets the text value of the flower category name
     * @param flowerCategoryName contains the text value of the flower category name
     *      
     */

    public void setFlowerCategoryName(String flowerCategoryName);

    /**
     * The method returns the name of the flower category
     * @param fc parameter contains the value of the flower category
     * @return name of the flower category that the method should return
     *      
     */

    public String getCategoryNameByCategory(EFlowerCategory fc);

    /**
     * The method returns the name of the flower color
     * @param fc parameter contains the color value of the flower
     * @return the name of the flower color that the method should return
     *      
     */

    public String getColorNameByColor(EFlowerColor fc);

}
