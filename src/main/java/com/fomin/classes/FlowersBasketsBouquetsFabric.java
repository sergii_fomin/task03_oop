package com.fomin.classes;

/**
 * Created by Fomin
 */

import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that implements a factory of objects for the application
 */
public class FlowersBasketsBouquetsFabric {

    // list of flower bouquets
    private List<FlowersBouquet> flowersBouquetList = new ArrayList<FlowersBouquet>();

    // list of flower baskets for forming bouquets
    private List<FlowersBasket> flowersBasketList = new ArrayList<FlowersBasket>();

    /**
     * Factory default constructor
     */
    public FlowersBasketsBouquetsFabric() {

    }

    public List<FlowersBouquet> getFlowersBouquetList() {
        return flowersBouquetList;
    }

    public void setFlowersBouquetList(List<FlowersBouquet> flowersBouquetList) {
        flowersBouquetList = flowersBouquetList;
    }

    public List<FlowersBasket> getFlowersBasketList() {
        return flowersBasketList;
    }

    public void setFlowersBasketList(List<FlowersBasket> flowersBasketList) {
        flowersBasketList = flowersBasketList;
    }

    /**
     * The method creates a flower object for a basket of flowers
     * @param flowerCode contains the value of the flower code
     * @param flowerName contains the name of the flower
     * @param flowerColorName contains the name of the flower color
     * @param flowerColor contains the color of the flower
     * @param flowerCategoryName contains the name of the flower category
     * @param flowerCategory contains the flower category
     * @param flowerCost contains the price of a flower
     * @return returns a flower object for a basket of flowers
     */
    public Flower CreateFlower(int flowerCode, String flowerName,
                               String flowerColorName, EFlowerColor flowerColor,
                               String flowerCategoryName, EFlowerCategory flowerCategory,
                               float flowerCost) {
        return new Flower(flowerCode, flowerName, flowerColorName, flowerColor, flowerCategoryName, flowerCategory, flowerCost);
    }

    /**
     * The method creates an object basket of flowers and places it in the list of flower baskets of the current factory of objects
     * @param flowersBasketCode contains the code for the flower basket object
     * @param flowersBasketName contains the name of the flower basket
     * @return returns a flower basket object
     */
    public FlowersBasket CreateFlowersBasket(int flowersBasketCode, String flowersBasketName) {
        FlowersBasket result = new FlowersBasket(flowersBasketCode, flowersBasketName);

        flowersBasketList.add(result);

        return result;
    }

    /**
     * The method creates an object with a bouquet of flowers and places it in the list of bouquets of the current factory of objects
     * @param flowersBouquetCode contains the code for the bouquet of flowers
     * @return returns a bunch of flowers object
     */
    public FlowersBouquet CreateFlowersBouquet(int flowersBouquetCode) {
        FlowersBouquet result = new FlowersBouquet(flowersBouquetCode);

        flowersBouquetList.add(result);

        return result;
    }

    /**
     * The method calculates the total cost of flower baskets
     * @return returns the total cost of flower baskets
     */
    public float calculateFlowersBasketListCost() {
        float result = 0.00f;

        if (flowersBasketList.size() > 0) {
            for (FlowersBasket basket : flowersBasketList) {
                result += basket.getFullCost();
            }
        }

        return result;
    }

    /**
     * The method calculates the total cost of bouquets of flowers
     * @return returns the total cost of bouquets of flowers
     */
    public float calculateFlowersBouquetListCost() {
        float result = 0.00f;

        if (flowersBouquetList.size() > 0) {
            for (FlowersBouquet bouquet : flowersBouquetList) {
                result += bouquet.getFullCost();
            }
        }

        return result;
    }

    /**
     * The method returns the basket index in the list of color baskets
     * @param flowersBasketList contains a list of baskets in which to search
     * @param fCode contains the basket code value in the list
     * @return returns the index of the basket in the list
     */
    public static int searchBasketIndexByCode(List<FlowersBasket> flowersBasketList, int fCode) {
        int i = -1;

        if (flowersBasketList.size() > 0) {
            i = 0;

            for (FlowersBasket basket : flowersBasketList) {
                if (fCode == basket.getFlowersBasketCode()) {
                    break;
                }
                i++;
            }
        }

        return i;
    }

    /**
     * The method returns the index of the bouquet in the list of flower bouquets
     * @param flowersBouquetList contains a list of bouquets in which to search
     * @param fCode contains the bouquet code value in the list
     * @return returns the index of the bouquet in the list
     */
    public static int searchBouquetIndexByCode(List<FlowersBouquet> flowersBouquetList, int fCode) {
        int i = -1;

        if (flowersBouquetList.size() > 0) {
            i = 0;

            for (FlowersBouquet bouquet : flowersBouquetList) {
                if (fCode == bouquet.getFlowersBouquetCode()) {
                    break;
                }
                i++;
            }
        }

        return i;
    }

    /**
      * Method for finding a flower of a certain category, color and value in a specified basket of flowers
      * @param fromFlwBasket parameter indicates which basket to look for
      * @param fCategory parameter indicates the category of flower from EFlowerCategory
      * @param fFlowerName parameter contains the name of the flower
      * @param fColor parameter indicates the color of the flower from EFlowerColor
      * @param fColorName parameter contains the name of the flower color
      * @param fCost parameter indicates the value of the flower
      * @return returns the index of the flower in the list of colors in the basket
      */
    public static int searchFlowerIndexFromFlwBasket(FlowersBasket fromFlwBasket, EFlowerCategory fCategory,
                                                     String fFlowerName, EFlowerColor fColor, String fColorName, float fCost) {
        int i = -1;

        if (fromFlwBasket.getFlowersBasketList().size() > 0) {
            i = 0;

            for (Flower flower : fromFlwBasket.getFlowersBasketList()) {
                if (fColor == EFlowerColor.OTHER_COLOR) {
                    if (flower.getFlowerCategory()== fCategory
                            && flower.getFlowerName().equals(fFlowerName)
                            && flower.getFlowerColor() == fColor
                            && flower.getFlowerColorName().equals(fColorName)
                            && flower.getFlowerCost() == fCost) {

                        break;
                    }
                } else {
                    if (flower.getFlowerCategory()== fCategory
                            && flower.getFlowerName().equals(fFlowerName)
                            && flower.getFlowerColor() == fColor
                            && flower.getFlowerCost() == fCost) {

                        break;
                    }
                }

                i++;
            }
        }

        return i;
    }

    /**
      * Method for finding a flower of a certain category, color and value in a specified bouquet of flowers
      * @param fromFlwBouquet parameter indicates which bouquet to look for
      * @param fCategory parameter indicates the category of flower from EFlowerCategory
      * @param fFlowerName parameter contains the name of the flower
      * @param fColor parameter indicates the color of the flower from EFlowerColor
      * @param fColorName parameter contains the name of the flower color
      * @param fCost parameter indicates the value of the flower
      * @return returns the flower index in the list of flowers in a bouquet
      */
    public static int searchFlowerIndexFromFlwBouquet(FlowersBouquet fromFlwBouquet, EFlowerCategory fCategory,
                                                      String fFlowerName, EFlowerColor fColor, String fColorName, float fCost) {
        int i = -1;

        if (fromFlwBouquet.getFlowersBouquetList().size() > 0) {
            i = 0;

            for (Flower flower : fromFlwBouquet.getFlowersBouquetList()) {
                if (fColor == EFlowerColor.OTHER_COLOR) {
                    if (flower.getFlowerCategory()== fCategory
                            && flower.getFlowerName().equals(fFlowerName)
                            && flower.getFlowerColor() == fColor
                            && flower.getFlowerColorName().equals(fColorName)
                            && flower.getFlowerCost() == fCost) {

                        break;
                    }
                } else {
                    if (flower.getFlowerCategory()== fCategory
                            && flower.getFlowerName().equals(fFlowerName)
                            && flower.getFlowerColor() == fColor
                            && flower.getFlowerCost() == fCost) {

                        break;
                    }
                }

                i++;
            }
        }

        return i;
    }

    /**
     * The method adds flowers from the specified basket to the bouquet
     * @param fromFlwBasket parameter indicates from which basket we select flowers
     * @param toFlwBouquet parameter indicates in which bouquet we will put the flowers
     * @param fCategory parameter indicates which category of flowers should be taken from the basket
     * @param fFlowerName parameter contains the name of the flower to be taken from the basket
     * @param fColor parameter indicates what color flowers should be taken from the basket
     * @param fColorName parameter indicates the name of what color the flowers should be taken from the basket
     * @param fCost parameter indicates at what price you need to take flowers from the basket
     * @param fCount parameter indicates the number of colors to be taken from the basket
     */
    public static void addFlowerToBouquet(FlowersBasket fromFlwBasket, FlowersBouquet toFlwBouquet,
                                          EFlowerCategory fCategory, String fFlowerName, EFlowerColor fColor, String fColorName,
                                          float fCost, int fCount) {

        int currentIndexFlower;

        for (int i = 0; i < fCount; i++) {
            currentIndexFlower = searchFlowerIndexFromFlwBasket(fromFlwBasket, fCategory, fFlowerName, fColor, fColorName, fCost);

            if (currentIndexFlower == -1) {
                System.out.println("Цветок с такими параметрами отсутствует в корзине!");
                break;
            } else {
                // add a flower from the basket to the bouquet
                toFlwBouquet.addFlower(fromFlwBasket.getFlowerByItemIndex(currentIndexFlower));
                // remove this flower from the basket
                fromFlwBasket.removeFlower(currentIndexFlower);
            }
        }
    }

    /**
     * The method returns flowers from the specified bouquet to the specified flower basket
     * @param fromFlwBouquet parameter indicates from which bouquet we select flowers
     * @param toFlwBasket parameter indicates to which basket we return flowers
     * @param fCategory parameter indicates which category of flowers should be taken from the bouquet
     * @param fFlowerName parameter contains the name of the flower to be taken from the bouquet
     * @param fColor parameter indicates what color flowers should be taken from a bouquet
     * @param fColorName parameter indicates the name of what color flowers should be taken from a bouquet
     * @param fCost parameter indicates at what price you need to take flowers from a bouquet
     * @param fCount parameter indicates the number of colors to be taken from the bouquet
     */
    public static void backFlowerToBasket(FlowersBouquet fromFlwBouquet, FlowersBasket toFlwBasket,
                                          EFlowerCategory fCategory, String fFlowerName,
                                          EFlowerColor fColor, String fColorName, float fCost, int fCount) {

        int currentIndexFlower;

        for (int i = 0; i < fCount; i++) {
            currentIndexFlower = searchFlowerIndexFromFlwBouquet(fromFlwBouquet, fCategory, fFlowerName, fColor, fColorName, fCost);

            if (currentIndexFlower == -1) {
                System.out.println("A flower with such parameters is not in the bouquet!");
                break;
            } else {
                // add a flower from the bouquet to the basket of flowers
                toFlwBasket.addFlower(fromFlwBouquet.getFlowerByItemIndex(currentIndexFlower));
                // remove this flower from the bouquet
                fromFlwBouquet.removeFlower(currentIndexFlower);
            }
        }
    }

    /**
      * The method displays information about the list of flower baskets in the console
      */
    public void printBasketsFromListBasket() {
        StringBuilder printInfo = new StringBuilder();

        printInfo.append("\nВсього горщиків с квітами: ")
                .append(flowersBasketList.size())
                .append("\nОбщая вартість горщиків с квітами: ")
                .append(calculateFlowersBasketListCost())
                .append(" \nСписок горщиків складає: ");

        for (FlowersBasket basket : this.flowersBasketList) {
            printInfo.append("\n Code: ")
                    .append(basket.getFlowersBasketCode())
                    .append(" Назва горщика: ")
                    .append(basket.getFlowersBasketName())
                    .append(" Кількість квітів у горщику: ")
                    .append(basket.getFlowersBasketCount())
                    .append(" Вартість горщика: ")
                    .append(basket.getFullCost());
        }

        System.out.println(printInfo);
    }

    /**
     * The method displays information on the list of flower bouquets to the console
     */
    public void printBouquetFromListBouquet() {
        StringBuilder printInfo = new StringBuilder();

        printInfo.append("\nВсього букетів с квітами: ")
                .append(flowersBouquetList.size())
                .append("\nЗагальна вартість букетів з квітами: ")
                .append(calculateFlowersBouquetListCost())
                .append("\nСписок букетів складається з: ");

        for (FlowersBouquet bouquet : this.flowersBouquetList) {
            printInfo.append("\nCode: ")
                    .append(bouquet.getFlowersBouquetCode())
                    .append(" Кількість квітів у букеті: ")
                    .append(bouquet.getFlowersBouquetCount())
                    .append(" Вартість букета: ")
                    .append(bouquet.getFullCost());
        }

        System.out.println(printInfo);
    }

}
