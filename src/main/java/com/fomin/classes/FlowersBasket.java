package com.fomin.classes;

/**
 * Created by Fomin
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Class basket of flowers, which is used to form bouquets of flowers
 */
public class FlowersBasket implements Comparable <FlowersBasket> {

    // Flower Basket Code
    private int flowersBasketCode;

    // Name of flower basket
    private String flowersBasketName;

    // List of flowers in the basket
    private List<Flower> flowersBasketList = new ArrayList<Flower>();

    // The cost of flowers in the basket
    private float fullCost = 0.00f;

    /**
     * Default constructor
     */
    public FlowersBasket() {
    }

    /**
      * Constructor to create a flower basket object
      * @param flowersBasketCode contains the basket code
      * @param flowersBasketName contains the name of the basket
      */
    public FlowersBasket(int flowersBasketCode, String flowersBasketName) {
        this.flowersBasketCode = flowersBasketCode;
        this.flowersBasketName = flowersBasketName;
    }

    /**
      * Constructor to create a flower basket object
      * @param flowersBasketCode contains the basket code
      * @param flowersBasketName contains the name of the basket
      * @param fbl contains a list of colors in the basket
      */
    public FlowersBasket(int flowersBasketCode, String flowersBasketName, List<Flower> fbl) {
        this.flowersBasketList = fbl;
        this.flowersBasketCode = flowersBasketCode;
        this.flowersBasketName = flowersBasketName;
    }

    /**
      * The method returns a list of color objects in the basket
      * @return returns an array of color objects contained in a color basket
      */
    public List<Flower> getFlowersBasketList() {
        return flowersBasketList;
    }

    /**
     * The method loads into the basket a new list of color objects
     * @param fal parameter contains a list of color objects
     */
    public void setFlowerBasketList(List<Flower> fal) {
        this.flowersBasketList = fal;
    }

    /**
     * The method adds a flower to the basket
     * @param flw contains a flower object that will be added to the basket
     */
    public void addFlower(Flower flw) {
        flowersBasketList.add(flw);
        calculateBasketCost();
    }

    /**
      * The method removes the flower object from the basket by index in the list
      * @param indexFlower contains the index value of the object in the list to be deleted
      */
    public void removeFlower(int indexFlower) {
        flowersBasketList.remove(indexFlower);
        calculateBasketCost();
    }

    /**
      * The method returns the flower object from the basket by index in the list
      * @param itemIndex contains the index value in the list
      * @return returns the flower object
      */
    public Flower getFlowerByItemIndex(int itemIndex) {
        return flowersBasketList.get(itemIndex);
    }

    /**
     * The method calculates the total cost of flowers in the basket
     */
    public void calculateBasketCost() {
        float tmpCost = 0.00f;

        if (flowersBasketList.size() > 0) {
            for (Flower flower : flowersBasketList) {
                tmpCost += flower.getFlowerCost();
            }
        }

        this.fullCost = tmpCost;
    }

    /**
     * The method returns the number of color objects in the basket.
     * @return returns the number of flowers in the basket
     */
    public int getFlowersBasketCount() {
        return this.flowersBasketList.size();
    }

    /**
     * The method returns the cost of flowers in the basket
     * @return returns the cost of flowers in a basket
     */
    public float getFullCost() {
        return fullCost;
    }

    /**
      * Overridden method for comparing the basket value of the current item
      * with the cost of the basket of the object transferred for comparison
      * @param obj contains an object to compare with the current object
      * @return returns the result of comparing the cost of baskets
      */
    @Override
    public int compareTo(FlowersBasket obj) {
        return Float.compare(this.fullCost, obj.fullCost);
    }

    public String getFlowersBasketName() {
        return flowersBasketName;
    }

    public void setFlowersBasketName(String flowersBasketName) {
        this.flowersBasketName = flowersBasketName;
    }

    public int getFlowersBasketCode() {
        return flowersBasketCode;
    }

    public void setFlowersBasketCode(int flowersBasketCode) {
        this.flowersBasketCode = flowersBasketCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlowersBasket)) return false;

        FlowersBasket that = (FlowersBasket) o;

        if (flowersBasketCode != that.flowersBasketCode) return false;
        if (Float.compare(that.fullCost, fullCost) != 0) return false;
        if (!flowersBasketName.equals(that.flowersBasketName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = flowersBasketCode;
        result = 31 * result + flowersBasketName.hashCode();
        result = 31 * result + (fullCost != +0.0f ? Float.floatToIntBits(fullCost) : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getName()
                + " Code: " + getFlowersBasketCode()
                + " Назва горщика: " + getFlowersBasketName()
                + " Кількість квітів у норщику: " + getFlowersBasketCount()
                + " Загальна вартість квітів у горщику: " + getFullCost();
    }

    public void printFlowersFromBasket() {
        StringBuilder printInfo = new StringBuilder();

        printInfo.append("Список квітів в горщику " + getFlowersBasketName() + ": \n");
        for (Flower flower : this.flowersBasketList) {
            printInfo.append("Code: " + flower.getFlowerCode())
                    .append(" Категорія и колір: " + flower.getFlowerName() + " (" + flower.getFlowerColorName() + ")")
                    .append(" Ціна:" + flower.getFlowerCost() + "\n");
        }

        System.out.println(printInfo);
    }
}
