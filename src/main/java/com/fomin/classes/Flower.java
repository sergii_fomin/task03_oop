package com.fomin.classes;

/**
 * Created by Fomin
 */

import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;
import com.fomin.interfaces.IFlowerForSale;

/**
 * The main class that implements work with flower objects for a bouquet
 */
public class Flower extends AFlower implements IFlowerForSale, Comparable <Flower> {
    /**
     * Flower code
     */
    private int flowerCode;
    /**
     * Name of flower
     */
    private String flowerName;

    /**
     * Flower color
     */
    private EFlowerColor flowerColor;
    /**
     * Flower color in text form
     */
    private String flowerColorName;

    /**
     * Flower category
     */
    private EFlowerCategory flowerCategory;
    /**
     * Flower category in text form
     */
    private String flowerCategoryName;

    /**
     * Flower cost
     */
    private float flowerCost;

    /**
                 * Designer to create a flower object
     * @param flowerCode parameter contains the flower code
     * @param flowerName parameter contains the name of the flower
     * @param flowerColorName parameter contains the name of the flower color
     * @param flowerColor parameter contains flower color
     * @param flowerCategory parameter contains the flower category
     * @param flowerCategoryName parameter contains the name of the flower category
     * @param flowerCost parameter contains the price of the flower
     */
    public Flower (int flowerCode, String flowerName,
                   String flowerColorName, EFlowerColor flowerColor,
                   String flowerCategoryName, EFlowerCategory flowerCategory,
                   float flowerCost) {
        // call the parent constructor to create the object
        super(flowerCode, flowerName, flowerColorName, flowerColor, flowerCategoryName, flowerCategory);

        // enter the value of the flower price
        this.flowerCost = flowerCost;
    }

    /**
     * The method returns the value of the flower price
     * @return returns the price value of a flower
     */
    public float getFlowerCost() {
        return flowerCost;
    }

    /**
     * The method allows you to enter the value of the price of a flower
     * @param flowerCost contains the price value of the flower
     */
    public void setFlowerCost(float flowerCost) {
        this.flowerCost = flowerCost;
    }

    /**
     * The method allows you to compare the identity of the current object of the flower class with the transferred for comparison
     * @param obj parameter contains an object to compare
     * @return comparison result
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Flower)) return false;

        Flower flower = (Flower) obj;

        if (this.flowerCode != flower.flowerCode) return false;
        if (this.flowerCategory != flower.flowerCategory) return false;
        if (!this.flowerCategoryName.equals(flower.flowerCategoryName)) return false;
        if (this.flowerColor != flower.flowerColor) return false;
        if (!this.flowerColorName.equals(flower.flowerColorName)) return false;
        if (!this.flowerName.equals(flower.flowerName)) return false;
        if (this.flowerCost != flower.flowerCost) return false;

        return true;
    }

    /**
     * The method returns information about the object of the flower class
     * @return contains detailed information about the current object
     */
    @Override
    public String toString() {
        return getClass().getName() + " "
                + "Code: " + getFlowerCode()
                + " Категорія и колір: " + getFlowerName() + " (" + getFlowerColorName()+ ")"
                + " Ціна:" + getFlowerCost();
    }


    /**
     * Method generates a hashcode of the flower object
     * @return returns a hashcode of the flower object
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (flowerCost != +0.0f ? Float.floatToIntBits(flowerCost) : 0);
        return result;
    }

    /**
     * Overridden method for comparing the value of the current flower object
     * with the value of the object flower transferred for comparison
     * @param obj contains an object to compare with the current object
     * @return returns the result of comparing the cost of colors
     */
    @Override
    public int compareTo(Flower obj) {
        return Float.compare(this.flowerCost, obj.flowerCost);
    }

}