package com.fomin.classes;

/**
 * Created by Fomin
 */

import java.util.ArrayList;
import java.util.List;

/**
 * The class contains an implementation for the object a bouquet of flowers
 */
public class FlowersBouquet implements Comparable <FlowersBouquet> {

    // Code of a bouquet of flowers
    private int flowersBouquetCode;

    // list of flowers in a bouquet
    private List<Flower> flowersBouquetList = new ArrayList<Flower>();

    // cost a bunch of flowers
    private float fullCost = 0.00f;

    /**
     * Default constructor
     */
    public FlowersBouquet() {
    }

    /**
     * Сonstructor to create an object bouquet of flowers
     * @param flowersBouquetCode contains a list of flowers in a bouquet
     */
    public FlowersBouquet(int flowersBouquetCode) {
        this.flowersBouquetCode = flowersBouquetCode;
    }

    /**
     * Сonstructor to create an object bouquet of flowers
     * @param flowersBouquetCode contains the code for the bouquet of flowers
     * @param fbl                contains a list of flowers in a bouquet
     */
    public FlowersBouquet(int flowersBouquetCode, List<Flower> fbl) {
        this.flowersBouquetCode = flowersBouquetCode;
        this.flowersBouquetList = fbl;
    }

    /**
     * The method returns a list of flower objects in a bouquet
     * @return returns a list of flowers in a bouquet
     */
    public List<Flower> getFlowersBouquetList() {
        return flowersBouquetList;
    }

    /**
     * The method loads a new list of flower objects into the bouquet
     * @param fal parameter contains a list of color objects
     */
    public void setFlowersBouquetList(List<Flower> fal) {
        this.flowersBouquetList = fal;
    }

    /**
     * The method adds a flower to the bouquet
     * @param flw contains a flower object that will be added to the bouquet
     */
    public void addFlower(Flower flw) {
        flowersBouquetList.add(flw);
        calculateBouquetCost();
    }

    /**
     * The method removes the flower object from the bouquet by index in the list
     * @param indexFlower contains the index value of the object in the list to be deleted
     */
    public void removeFlower(int indexFlower) {
        flowersBouquetList.remove(indexFlower);
        calculateBouquetCost();
    }

    /**
     * The method calculates the cost of the bouquet
     */
    public void calculateBouquetCost() {
        float tmpCost = 0.00f;

        if (flowersBouquetList.size() > 0) {
            for (Flower flower : flowersBouquetList) {
                tmpCost += flower.getFlowerCost();
            }
        }

        this.fullCost = tmpCost;
    }

    /**
     * The method returns the flower object from the basket by index in the list
     * @param itemIndex contains the index value in the list
     * @return returns the flower object
     */
    public Flower getFlowerByItemIndex(int itemIndex) {
        return flowersBouquetList.get(itemIndex);
    }

    /**
     * The method returns the number of color objects in the bouquet
     * @return the number of flowers in a bouquet
     */
    public int getFlowersBouquetCount() {

        return this.flowersBouquetList.size();
    }

    /**
     * The method returns the cost of the bouquet
     * @return returns the cost of the bouquet
     */
    public float getFullCost() {
        return fullCost;
    }

    public int getFlowersBouquetCode() {
        return flowersBouquetCode;
    }

    public void setFlowersBouquetCode(int flowersBouquetCode) {
        this.flowersBouquetCode = flowersBouquetCode;
    }

    /**
     * Overridden method for comparing the cost of a bouquet of the current object
     * with the cost of the bouquet of the object transferred for comparison
     * @param obj contains an object to compare with the current object
     * @return returns the result of comparing the cost of baskets
     */
    @Override
    public int compareTo(FlowersBouquet obj) {
        return Float.compare(this.fullCost, obj.fullCost);
    }

    @Override
    public String toString() {
        return getClass().getName()
                + " Код: " + getFlowersBouquetCode()
                + " Кількість квітів у букеті: " + getFlowersBouquetCount()
                + " Загальна вартість квітів у букеті: " + getFullCost();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlowersBouquet)) return false;

        FlowersBouquet that = (FlowersBouquet) o;

        if (flowersBouquetCode != that.flowersBouquetCode) return false;
        if (Float.compare(that.fullCost, fullCost) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = flowersBouquetCode;
        result = 31 * result + (fullCost != +0.0f ? Float.floatToIntBits(fullCost) : 0);
        return result;
    }

    public void printFlowersFromBouquet() {
        StringBuilder printInfo = new StringBuilder().append("\nСписок квітів в букеті № " + this.getFlowersBouquetCode() + ": ");

        for (Flower flower : this.flowersBouquetList) {
            printInfo.append("\nКод: " + flower.getFlowerCode())
                    .append(" Категорія іі колір: " + flower.getFlowerName() + " (" + flower.getFlowerColorName()+ ")")
                    .append(" Ціна:" + flower.getFlowerCost() + "\n");
        }

        System.out.println(printInfo);
    }
}