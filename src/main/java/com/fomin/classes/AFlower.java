package com.fomin.classes;

/**
 * Created by Fomin
 */

import com.fomin.enums.EFlowerCategory;
import com.fomin.enums.EFlowerColor;
import com.fomin.interfaces.IFlower;

/**
 * Abstract class for inheriting Flower objects
 */
public abstract class AFlower implements IFlower {
    /**
     * Flower code
     */
    int flowerCode;
    /**
     * Name of flower
     */
    String flowerName;

    /**
     * Flower color
     */
    EFlowerColor flowerColor;
    /**
     * Flower color in text form
     */
    String flowerColorName;

    /**
     * Flower category
     */
    EFlowerCategory flowerCategory;
    /**
     * Flower category in text form
     */
    String flowerCategoryName;

    /**
     * Default constructor
     */
    public AFlower() {
    }

    /**
     * Designer to create a flower object
     * @param flowerCode parameter contains the flower code
     * @param flowerName parameter contains the name of the flower
     * @param flowerColor parameter contains flower color
     * @param flowerCategory parameter contains the flower category
     */
    public AFlower(int flowerCode, String flowerName, EFlowerColor flowerColor, EFlowerCategory flowerCategory){
        this.flowerCode = flowerCode;
        this.flowerName = flowerName;
        this.flowerColor = flowerColor;
        this.flowerCategory = flowerCategory;
    }

    /**
     * Designer to create a flower object
     * @param flowerCode parameter contains the flower code
     * @param flowerName parameter contains the name of the flower
     * @param flowerColorName parameter contains the name of the flower color
     * @param flowerColor parameter contains flower color
     * @param flowerCategoryName parameter contains the name of the flower category
     * @param flowerCategory parameter contains the flower category
     */
    public AFlower(int flowerCode, String flowerName,
                   String flowerColorName, EFlowerColor flowerColor,
                   String flowerCategoryName, EFlowerCategory flowerCategory){
        this.flowerCode = flowerCode;
        this.flowerName = flowerName;
        this.flowerColor = flowerColor;
        this.flowerColorName = flowerColorName;
        this.flowerCategory = flowerCategory;
        this.flowerCategoryName = flowerCategoryName;
    }

    /**
     * Returns the value of the flower code
     * @return contains the value of the flower code
     */
    public int getFlowerCode() {
        return flowerCode;
    }

    /**
     * Sets the value of the flower code
     * @param flowerCode contains the value of the flower code
     */
    public void setFlowerCode(int flowerCode) {
        this.flowerCode = flowerCode;
    }

    /**
     * Returns the value of a flower name
     * @return contains the name of the flower
     */
    public String getFlowerName() {
        return flowerName;
    }

    /**
     * Sets the value of a flower name
     * @param flowerName contains the value of the flower name
     */
    public void setFlowerName(String flowerName) {
        this.flowerName = flowerName;
    }

    /**
     * Returns the color value of a flower
     * @return contains the color value of the flower
     */
    public EFlowerColor getFlowerColor() {
        return flowerColor;
    }

    /**
     * Sets the color value of the flower
     * @param flowerColor contains the color value of the flower
     */
    public void setFlowerColor(EFlowerColor flowerColor) {
        this.flowerColor = flowerColor;
    }

    /**
     * Returns the text value of the flower color name
     * @return contains the text value of the flower color name
     */
    public String getFlowerColorName() {
        return flowerColorName;
    }

    /**
     * Sets the text value of the flower color name
     * @param flowerColorName contains the text value of the flower color name
     */
    public void setFlowerColorName(String flowerColorName) {
        if (this.flowerColorName == null || this.flowerColorName.isEmpty())
            this.flowerColorName = getColorNameByColor(this.flowerColor);
        else this.flowerColorName = flowerColorName;
    }

    /**
     * Returns the value of a flower category
     * @return contains flower category value
     */
    public EFlowerCategory getFlowerCategory() {
        return flowerCategory;
    }

    /**
     * Sets the value of a flower category
     * @param flowerCategory contains the value of the flower category
     */
    public void setFlowerCategory(EFlowerCategory flowerCategory) {
        this.flowerCategory = flowerCategory;
    }

    /**
     * Returns the text value of a flower category name
     * @return contains the text value of the flower category name
     */
    public String getFlowerCategoryName() {
        return flowerCategoryName;
    }

    /**
     * Sets the text value of the flower category name
     * @param flowerCategoryName contains the text value of the flower category name
     */
    public void setFlowerCategoryName(String flowerCategoryName) {
        if (this.flowerCategoryName == null || this.flowerCategoryName.isEmpty())
            this.flowerCategoryName = getCategoryNameByCategory(this.flowerCategory);
        else this.flowerCategoryName = flowerCategoryName;
    }

    /**
     * Method can be overridden in inherited classes
     * Returns the name of a flower category
     * @param fc parameter contains the value of the flower category
     * @return name of the flower category that the method should return
     */
    public String getCategoryNameByCategory(EFlowerCategory fc) {
        return EFlowerCategory.getNameByCategory(fc);
    }

    /**
     * Method can be overridden in inherited classes
     * Returns the name of the flower color
     * @param fc parameter contains the color value of the flower
     * @return the name of the flower color that the method should return
     */
    public String getColorNameByColor(EFlowerColor fc) {
        return EFlowerColor.getColorNameByColor(fc);
    }

    /**
     * The method allows you to compare the identity of the current object of the flower class with the transferred for comparison
     * @param obj parameter contains an object to compare
     * @return comparison result
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof AFlower)) return false;

        AFlower aFlower = (AFlower) obj;

        if (flowerCode != aFlower.flowerCode) return false;
        if (flowerCategory != aFlower.flowerCategory) return false;
        if (!flowerCategoryName.equals(aFlower.flowerCategoryName)) return false;
        if (flowerColor != aFlower.flowerColor) return false;
        if (!flowerColorName.equals(aFlower.flowerColorName)) return false;
        if (!flowerName.equals(aFlower.flowerName)) return false;

        return true;
    }

    /**
     * Method generates a HashCode object
     * @return returns a HashCode of an object
     */
    @Override
    public int hashCode() {
        int result = flowerCode;
        result = 31 * result + flowerName.hashCode();
        result = 31 * result + flowerColor.hashCode();
        result = 31 * result + flowerColorName.hashCode();
        result = 31 * result + flowerCategory.hashCode();
        result = 31 * result + flowerCategoryName.hashCode();
        return result;
    }
}
